Git global setup
git config --global user.name "hym12"
git config --global user.email "qq104266250@126.com"

Create a new repository
git clone https://gitlab.com/hym12/h2.git
cd h2
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/hym12/h2.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/hym12/h2.git
git push -u origin --all
git push -u origin --tags